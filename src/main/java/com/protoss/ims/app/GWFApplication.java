package com.protoss.ims.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.WebApplicationInitializer;

import com.protoss.ims.app.spring.configuration.ApplicationConfiguration;
import com.protoss.ims.app.spring.configuration.ApplicationInitializer;

import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@ComponentScan(basePackages = "com.protoss.ims.app")
@EnableAutoConfiguration
@Import({ApplicationConfiguration.class})
public class GWFApplication extends SpringBootServletInitializer implements WebApplicationInitializer{

    static ConfigurableApplicationContext ctx;
    static ApplicationInitializer bean;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(GWFApplication.class);
    }

    /*Set Time Zone */

//    @PostConstruct
//    void started() {
//
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
//
//    }

    public static void main(String[] args) {
        ctx = SpringApplication.run(GWFApplication.class, args);
        bean = ctx.getBean(ApplicationInitializer.class);
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
    }
}
