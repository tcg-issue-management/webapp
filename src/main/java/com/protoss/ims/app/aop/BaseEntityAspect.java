package com.protoss.ims.app.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.protoss.ims.app.spring.security.CustomUserModel;
import com.protoss.ims.app.util.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Map;

@Component
@Aspect
public class BaseEntityAspect {
	static final Logger LOGGER = LoggerFactory.getLogger(BaseEntityAspect.class);

	@Autowired
    CustomUserModel customUserModel;
	

    private ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };


    @Pointcut("execution(* com.spt.app.service.*.save*(..)) || execution(* com.spt.app.service.*.update*(..))")
    public void stampUserMethods() {
    }


    @Before("stampUserMethods()")
    public void stampUserMethods(JoinPoint jp) {
    	LOGGER.info(" ---Start AOP BaseEntity ---");
    	try {
			String username = (String) customUserModel.getValue("userName");
			String methodName = jp.getSignature().getName();
			for(Object arg:jp.getArgs()){
				if(arg instanceof Map ){
					Map<String,Object> map = (Map<String,Object>)arg;
					if(map.get("id")!=null && ((String[]) map.get("id"))[0].trim().length() > 0){
						LOGGER.info(" --== Case Update ==--");
						map.put("updateBy", new String[] { username } );
						map.put("updateDate", new String[] { String.valueOf(DateUtil.getCurrentDate()) });
					}else{
						LOGGER.info(" --== Case Save ==--");
						map.put("createdBy", new String[] { username });
						map.put("createdDate", new String[] { String.valueOf(DateUtil.getCurrentDate()) });
					}
				}
			}
		} catch (Exception e) {
			LOGGER.info("NO Principal");
		}
		LOGGER.info(" ---End AOP BaseEntity ---");
    }

}
