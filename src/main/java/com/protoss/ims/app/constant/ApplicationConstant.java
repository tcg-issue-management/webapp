package com.protoss.ims.app.constant;

import java.text.SimpleDateFormat;
import java.util.*;

public class ApplicationConstant {

	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmm");
	public static final String APPLICATION_NAME    = "ISSUE MANAGEMENT";
	public static final String APPLICATION_VERSION = "v.1.0.20171105_1814";
	public static String APPLICATION_VERSION_CURRENT = "v.1.0."+sdf.format(new Date());

	public static  String FETCH_FRONTEND_PARAMETER_TIME = (new SimpleDateFormat("yyyyMMddhhmm")).format(new Date());
	public static final String FETCH_PARAMETER_NAME = "PARAMETER_FETCH";
	
	public static final String PARAMETER_PATH_IMAGE 			= "P100";
	public static final String PARAMETER_LANGUAGE 				= "P101";
	public static final String PARAMETER_DUEDATE_ADVANCE 		= "P104";
	public static final String PARAMETER_LINK_RESERVE_CAR 		= "P106";
	public static final String PARAMETER_HOTEL_RESERVATION 		= "P107";
	public static final String PARAMETER_FLIGHT_RESERVATION		= "P108";
	public static final String PARAMETER_CAR_RESERVATION		= "P109";

	/* create by siriradC. 2017.08.09 for page approve */
	public static final String MASTER_DATA_EXPENSE_TYPE		= "M001";
	public static final String MASTER_DATA_APPROVE_TYPE		= "M002";
	public static final String MASTER_DATA_REQUEST_STATUS	= "M005";
	public static final String MASTER_DATA_ACTION_REASON	= "M006";
	public static final String MASTER_DATA_DOCUMENT_TYPE	= "M008";
	public static final String MASTER_DATA_DOCUMENT_STATUS	= "M009";
	public static final String MASTER_DATA_CAR_TYPE			= "M010";
	public static final String MASTER_DATA_HOTEL			= "M011";
	public static final String MASTER_DATA_MILEAGE			= "M013";
	public static final String MASTER_DATA_AIRLINE			= "M014";
	public static final String MASTER_DATA_ATTACHMENT_TYPE	= "M015";
	public static final String MASTER_DATA_SPECIAL_DELEGATE	= "M026";

	public static final String JBPM_STATE_RETURN_APPROVER   = "Approver";
	public static final String JBPM_STATE_RETURN_FINANCE   = "Finance";
	public static final String JBPM_STATE_RETURN_ACCOUNT   = "Account";

	public static final String APPROVE_TYPE_DOMESTIC_CODE		= "0001";
	public static final String APPROVE_TYPE_FOREIGN_CODE		= "0002";
	public static final String APPROVE_TYPE_CAR_CODE			= "0003";
	public static final String APPROVE_TYPE_HOTEL_CODE			= "0004";
	public static final String APPROVE_TYPE_FLIGHT_BOOKING_CODE			= "0005";
	public static final String APPROVE_TYPE_DOMESTIC_SET_CODE			= "0006";
	public static final String APPROVE_TYPE_FOREIGN_SET_CODE			= "0007";
	public static final String APPROVE_TYPE_EXPENSE_OVER_RIGHT			= "Y";

	public static final String DOCUMENT_TYPE_APPROVE			= "APP";
	public static final String DOCUMENT_STATUS_COMPLETE			= "CMP";

	public static final String URL_EWF			= "http://ewf.mitrphol.com:8001/GWF/";



	/* Flow Type Code */
	public static final String FLOW_TYPE_CODE_APPROVE		= "APP";
	public static final String FLOW_TYPE_CODE_EXPENSE		= "EXP";
	

	/* for load master data and parameter to local storage */
	public static final String[] PARAMETER_LOAD_LOCAL = {
			PARAMETER_PATH_IMAGE,
			PARAMETER_LANGUAGE
	};

	public static final String[] MASTER_DATA_LOAD_LOCAL = {
			MASTER_DATA_EXPENSE_TYPE,
			MASTER_DATA_APPROVE_TYPE,
			MASTER_DATA_DOCUMENT_TYPE,
			MASTER_DATA_DOCUMENT_STATUS,
			MASTER_DATA_CAR_TYPE,
			MASTER_DATA_HOTEL,
			MASTER_DATA_MILEAGE
	};


	public static final Map<Integer,String> ROLE_ACTION_STATUS = new HashMap<Integer,String>(){{
		 put(0,    "VRF");
		 put(1,    "APR");
		 put(2,    "ACC");
		 put(3,    "FNC");
		 put(4,    "HR");
		 put(5,    "ADM");
		
	}};
	

	
	public static final Map<Integer,String> ROLE_ACTION_JBPM = new HashMap<Integer,String>(){{
		 put(0,    "v_person");
		 put(1,    "app_person");
		 put(2,    "acc_person");
		 put(3,    "fin_person");
		 put(4,    "hr_person");
		 put(5,    "adm_person");
		
	}};
	

	 
	public static final Map<String,String> ROLE_ACTION_UPDATE_JBPM = new HashMap<String,String>(){{
		 put("VRF",    "v_action_out");
		 put("APR",    "app_action_out");
		 put("ACC",    "acc_action_out");
		 put("FNC",    "fin_action_out");
		 put("HR",     "hr_act_out");
		 put("ADM",    "adm_act_out");
		
	}};
	

	 
	public static final Map<String,String> DOC_TYPE_TO_FLOW_DOC_TYPE = new HashMap<String,String>(){{
		 put("ADV",    "EXP");
		 put("APP",    "APP");
		 put("EXP",    "EXP");
		
	}};
	
	
	public static final Map<String,String> EXCEPTION_URL = new HashMap<String,String>(){{
		 put("qrcodescandata",    "qrcodescandata");
		
	}};
	
	/* Status In Flow */
	public static final String FLOW_STATUS_APPROVED 				= "A";
	public static final String FLOW_STATUS_REJECTED 				= "R";
	public static final String FLOW_STATUS_DEFAULT 				    = "Y";
	public static final String FLOW_STATUS_ACTIVE				    = "ACTIVE";
	public static final String FLOW_STATUS_INACTIVE				    = "NONE";


	/* Time Constant */
	public static final Integer TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN			= 24;
	public static final String  TIME_HOURS_PER_DAY_FOR_DAY_FROM_HOUR		= "24:00:00";
	public static final Integer TIME_MINUTE_PER_HOURS 		= 60;
	public static final Integer TIME_SECOND_PER_HOURS 		= 60;
	public static final Integer TIME_MILLI_SECOND 			= 1000;

	/* Calculate Function Parameter */
	public static final Integer	CALCULATE_DIFFERENCE_DAY	= (TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN * TIME_MINUTE_PER_HOURS * TIME_SECOND_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_DIFFERENCE_HOURS	= (TIME_MINUTE_PER_HOURS * TIME_SECOND_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_DIFFERENCE_MINUTE	= (TIME_MINUTE_PER_HOURS * TIME_MILLI_SECOND);
	public static final Integer	CALCULATE_CONVERT_HOURS_TO_MINUTE	= (TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN * TIME_MINUTE_PER_HOURS);

	/* Constant C Level of Employee */
	public static final Integer CLEVEL_09 = 9;
	public static final Integer CLEVEL_10 = 10;
	public static final Integer CLEVEL_11 = 11;

	/* Constant Flow Type name */
	public static final String FLOW_TYPE_H001 = "H001";
	public static final String FLOW_TYPE_H002 = "H002";
	public static final String FLOW_TYPE_H003 = "H003";
	public static final String FLOW_TYPE_H004 = "H004";
	public static final String FLOW_TYPE_H005 = "H005";

	/* Constant Location Type */
	public static final String LOCATION_TYPE_DOMESTIC 	= "D";
	public static final String LOCATION_TYPE_FOREIGN 	= "F";
	public static final String ZONE_CODE_ASEAN 			= "MM";

	public static final List<String> EXPENSE_TYPE_FOREIGN = new ArrayList<String>(){{
		add("E00044");
		add("E00045");
		add("E00046");
		add("E00048");
		add("E00049");
		add("E00050");
		add("E00051");
		add("E00052");
		add("E00055");
	}};
}
