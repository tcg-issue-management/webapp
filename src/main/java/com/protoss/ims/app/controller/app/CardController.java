package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.protoss.ims.app.service.BoardService;
import com.protoss.ims.app.service.CardsService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/card")
public class CardController {

    static final Logger LOGGER = LoggerFactory.getLogger(CardController.class);

    @Autowired
    CardsService cardsService;

    @GetMapping("/list")
    public String cardPage(ModelMap map) {
        //map.addAttribute("projectId", id);
        return "cardList";
    }


    @GetMapping("/findCardList")
    public ResponseEntity<String> findCardList(@RequestParam("id")Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = cardsService.findCardList(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/findCardsByProjectIdAndStatus")
    public ResponseEntity<String> findCardsByProjectIdAndStatus(@RequestParam("id")Long id,@RequestParam("status")String status) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = cardsService.findCardsByProjectIdAndStatus(id,status);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/addCard" ,method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> addCard(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            System.out.println("Json Data = "+json);
//            return new ResponseEntity<String>("test", headers, HttpStatus.OK);
            return cardsService.saveCard(json);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}