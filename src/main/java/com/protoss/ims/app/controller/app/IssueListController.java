package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/issue")
public class IssueListController {

    static final Logger LOGGER = LoggerFactory.getLogger(IssueListController.class);

    @GetMapping("/list")
    public String issueListPage(ModelMap map) {
        //map.addAttribute("projectId", id);
        return "issueList";
    }



}