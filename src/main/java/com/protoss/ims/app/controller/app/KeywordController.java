package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/keyword")
public class KeywordController {

    static final Logger LOGGER = LoggerFactory.getLogger(KeywordController.class);

    @GetMapping("/list")
    public String boardPage(ModelMap map) {
        //map.addAttribute("projectId", id);
        return "keywordList";
    }



}