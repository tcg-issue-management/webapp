package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.protoss.ims.app.service.ProjectService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    ProjectService projectService;


    @GetMapping("/list")
    public String projectPage(ModelMap map) {
        //map.addAttribute("projectId", id);
        return "projectList";
    }

    @GetMapping("/findAllProject")
    public ResponseEntity<String> findAllProject() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = projectService.findAllProject();
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/addProject" ,method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> addProject(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            System.out.println("Json Data = "+json);
//            return new ResponseEntity<String>("test", headers, HttpStatus.OK);
            return projectService.saveProject(json);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/addProjectGroup" ,method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> addProjectGroup(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            System.out.println("Json Data = "+json);
//            return new ResponseEntity<String>("test", headers, HttpStatus.OK);
            return projectService.saveProjectGroup(json);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
