package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.protoss.ims.app.service.BoardService;
import com.protoss.ims.app.service.CardsService;
import com.protoss.ims.app.service.ReportService;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;

@Controller
@RequestMapping("/report")
public class ReportController {

    static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);


    @Autowired
    ReportService reportService;

 
    @GetMapping(value="/generateReport",produces="application/json")
    @ResponseBody
    public ResponseEntity<String> generateReport(@RequestParam(value = "id", required = false) Long id,
                                                 @RequestParam(value = "startDate", required = false) String startDate,
                                                 @RequestParam(value = "endDate", required = false) String endDate,
                                                 @RequestParam(value = "status", required = false) String status,
                                                 HttpServletResponse response) throws IOException,ServletException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputByteStream = null;
        Map<String,String> mapMessage = new HashMap();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy-HHmm");
        String currentDate = df.format(c.getTime());

        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("Report_Summary_"+currentDate+".xlsx");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"Report_Summary_"+currentDate+".xlsx");


            byte[] result = reportService.generateReport(id,startDate,endDate,status);
            inputByteStream = new ByteArrayInputStream(result);
            IOUtils.copy(inputByteStream, response.getOutputStream());
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }






}