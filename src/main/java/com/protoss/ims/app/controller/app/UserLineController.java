package com.protoss.ims.app.controller.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.ims.app.service.UserLineService;

@Controller
@RequestMapping("/userLine")
public class UserLineController {

    static final Logger LOGGER = LoggerFactory.getLogger(UserLineController.class);

    @Autowired
    UserLineService userLineService;

    @GetMapping("/list")
    public String userLinePage(ModelMap map) {
        //map.addAttribute("projectId", id);
        return "userList";
    }


    @GetMapping("/findUserLine")
    public ResponseEntity<String> findCardList(@RequestParam("id")Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = userLineService.findUserList(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}