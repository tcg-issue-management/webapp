package com.protoss.ims.app.service;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Properties;

@Configuration
public abstract class AbstractReportService {
    protected static Logger LOGGER = LoggerFactory.getLogger(AbstractReportService.class);

    @Value("${ReportServer}")
    protected String ReportServer = "localhost:8080/Report/rest";
    protected static Properties connectProperties = null;

    protected String webServicesString = "";
    protected String resultString = "";

    protected RestTemplate restTemplate = new RestTemplate();
    protected JsonParser parser = new JsonParser();

    JsonSerializer<Date> ser = new JsonSerializer<Date>() {
        public JsonElement serialize(Date src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    public AbstractReportService() {
    }

    public String getWebServicesString() {
        return webServicesString;
    }

    public AbstractReportService setWebServicesString(String webServicesString) {
        this.webServicesString = webServicesString;
        return this;
    }

    public String getResultString() {
        LOGGER.debug("request :{}", getWebServicesString());
        resultString = restTemplate.getForObject(getWebServicesString(), String.class);
        return resultString;
    }

    public String getResultString(String url) {
        LOGGER.debug("request :{}", url);
        resultString = restTemplate.getForObject(url, String.class);
        return resultString;
    }


}
