package com.protoss.ims.app.service;

import org.springframework.http.ResponseEntity;

public interface BoardService {
    ResponseEntity<String> findGroupAndCardByProjectId(Long projectId);
}
