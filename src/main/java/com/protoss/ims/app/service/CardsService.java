package com.protoss.ims.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface CardsService {
    ResponseEntity<String> findCardList(Long id);
    ResponseEntity<String> findCardsByProjectIdAndStatus(Long id,String status);
    ResponseEntity<String> saveCard(String json);

}
