package com.protoss.ims.app.service;

import java.util.Set;

public interface DocumentAccessService {

    public Set<String> getDocumentAccessPerson(String docId);

}
