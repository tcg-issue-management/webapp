package com.protoss.ims.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface ProjectService {
    ResponseEntity<String> findAllProject();
    ResponseEntity<String> saveProject(String json);
    ResponseEntity<String> saveProjectGroup(String json);
}
