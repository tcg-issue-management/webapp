package com.protoss.ims.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ReportService {
   
    public byte[]  generateReport(Long id,
                                  String startDate,
                                  String endDate,
                                  String status
                                  );


}
