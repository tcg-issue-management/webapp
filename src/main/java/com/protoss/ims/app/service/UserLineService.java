package com.protoss.ims.app.service;

import org.springframework.http.ResponseEntity;

public interface UserLineService {
    ResponseEntity<String> findUserList(Long id);

}
