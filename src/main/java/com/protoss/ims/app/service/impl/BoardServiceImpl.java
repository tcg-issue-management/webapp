package com.protoss.ims.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.protoss.ims.app.service.AbstractEngineService;
import com.protoss.ims.app.service.BoardService;

@Service("BoardService")
public class BoardServiceImpl extends AbstractEngineService implements BoardService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findGroupAndCardByProjectId(Long projectId) {
        String url = "/api/groups/"+projectId;
        return getResultByExchange(url);
    }

}