package com.protoss.ims.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.protoss.ims.app.service.AbstractEngineService;
import com.protoss.ims.app.service.BoardService;
import com.protoss.ims.app.service.CardsService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@Service("CardsService")
public class CardsServiceImpl extends AbstractEngineService implements CardsService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }


    @Override
    public ResponseEntity<String> findCardList(Long id) {
        String url = "/cards/findCardsByProjectId?id="+id;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findCardsByProjectIdAndStatus(Long id, String status) {
        String url = "/cards/findCardsByProjectIdAndStatus?id="+id+"&status="+status;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> saveCard(String json) {
        LOGGER.info("####Service[saveCard] ####");
        RestTemplate restTemplate = new RestTemplate();
        String url = "/cards/saveCard";
        try {
            json = URLDecoder.decode(json, "UTF-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        return getResultStringByTypeHttpMethodAndBodyContent(json,HttpMethod.POST,url,restTemplate);
    }
}