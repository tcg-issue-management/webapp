package com.protoss.ims.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.protoss.ims.app.service.AbstractEngineService;
import com.protoss.ims.app.service.ProjectService;
import com.protoss.ims.app.util.HttpRequestParamConvertUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

@Service("ProjectService")
public class ProjectServiceImpl extends AbstractEngineService implements ProjectService {


    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findAllProject() {
        String url = "/api/project";
        return getResultByExchange(url);
    }


    @Override
    public ResponseEntity<String> saveProject(String json){

        LOGGER.info("####Service[saveProject] ####");
        RestTemplate restTemplate = new RestTemplate();
        String url = "/api/saveProject";
        try {
            json = URLDecoder.decode(json, "UTF-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        return getResultStringByTypeHttpMethodAndBodyContent(json,HttpMethod.POST,url,restTemplate);
    }

    @Override
    public ResponseEntity<String> saveProjectGroup(String json) {
        LOGGER.info("####Service[saveProjectGroup] ####");
        RestTemplate restTemplate = new RestTemplate();
        String url = "/api/groups/saveProjectGroup";
        try {
            json = URLDecoder.decode(json, "UTF-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        return getResultStringByTypeHttpMethodAndBodyContent(json,HttpMethod.POST,url,restTemplate);
    }


}
