package com.protoss.ims.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.protoss.ims.app.service.AbstractEngineService;
import com.protoss.ims.app.service.BoardService;
import com.protoss.ims.app.service.CardsService;
import com.protoss.ims.app.service.ReportService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service("ReportService")
public class ReportServiceImpl extends AbstractEngineService implements ReportService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public byte[] generateReport(Long id,String startDate,String endDate,String status) {
        Map<String,Object> jsonString = new HashMap();	
		String json = gson.toJson(jsonString);
		String url = "/report/generateReport?id="+id+"&startDate="+startDate+"&endDate="+endDate+"&status="+status;
		return printReportWithPost(json, HttpMethod.POST,url);
    }


  
}