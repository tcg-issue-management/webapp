package com.protoss.ims.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.protoss.ims.app.service.AbstractEngineService;
import com.protoss.ims.app.service.CardsService;
import com.protoss.ims.app.service.UserLineService;

@Service("UserLineService")
public class UserLineServiceImpl extends AbstractEngineService implements UserLineService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }




    @Override
    public ResponseEntity<String> findUserList(Long id) {
        String url = "/userLine/findUserLineByProjectId?id="+id;
        return getResultByExchange(url);
    }
}