package com.protoss.ims.app.spring.configuration;

import java.util.Locale;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.resource.FixedVersionStrategy;
import org.springframework.web.servlet.resource.VersionResourceResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import com.protoss.ims.app.constant.ApplicationConstant;
import com.protoss.ims.app.spring.localstorage.CookieLocalStorageInteceptor;

@EnableAutoConfiguration
@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.protoss.ims.app")
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {
	
	@Bean
	public ViewResolver viewResolver() {
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(TilesView.class);
	
		return viewResolver;
	}
	
	@Bean
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
	    resolver.setDefaultEncoding("utf-8");
	    return resolver;
	}
	
	@Bean
	public DocumentAuthorizationInteceptor documentAuthorizationInteceptor() {
	    return new DocumentAuthorizationInteceptor();
	}
	
	
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		VersionResourceResolver versionResourceResolver = new VersionResourceResolver()
		        .addVersionStrategy(new FixedVersionStrategy(ApplicationConstant.APPLICATION_VERSION_CURRENT), "/**");
		
		
		if (!registry.hasMappingForPattern("/resources/**")) {
			registry.addResourceHandler("/resources/**")
			.addResourceLocations("/")
			.addResourceLocations("classpath:/META-INF/web-resources/")
			.setCachePeriod(60 * 60 * 24 * 365) /* one year */
			.resourceChain(true)
			.addResolver(versionResourceResolver);
		}
	}



	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(new RememberFilterInteceptor()).addPathPatterns("/**/findByCriteria**").excludePathPatterns("/login**");
        registry.addInterceptor(new CookieMemoryFilterInteceptor()).addPathPatterns("/**/listView**").excludePathPatterns("/login**");
        registry.addInterceptor(new MemberLinkExcludeLangInteceptor()).addPathPatterns("/**").excludePathPatterns("/login**");
        registry.addInterceptor(cookieLocalStorageInteceptor()).addPathPatterns("/**").excludePathPatterns("/login**","/admin/**");
        registry.addInterceptor(documentAuthorizationInteceptor()).addPathPatterns(
    		  "/expense/clearExpenseDetail**",
    		  "/expense/expenseDetail**",
    		  "/advance/advanceDetail**",
    		  "/approve/viewCreateDocSetDetail**");
//        registry.addInterceptor(new UrlAuthorizationInteceptor()).addPathPatterns("/**").excludePathPatterns(
//        		"/login**",
//        		"/admin/**",
//        		"/chat/**",
//        		"/Access_Denied**");
    }
	
	@Bean
	public CookieLocalStorageInteceptor cookieLocalStorageInteceptor() {
		CookieLocalStorageInteceptor cookieLocalStorageInteceptor = new CookieLocalStorageInteceptor();
	
		return cookieLocalStorageInteceptor;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
	
		return localeChangeInterceptor;
	}
	
	@Bean
	public LocaleResolver localeResolver(){
		CookieLocaleResolver resolver=new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("en"));
		return resolver;
	} 
	
	@Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(	"WEB-INF/i18n/application",
        							"WEB-INF/i18n/label",
        							"WEB-INF/i18n/button",
        							"WEB-INF/i18n/message");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

	@Bean
    public CommonsMultipartResolver commonsMultipartResolver() {
		CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        return commonsMultipartResolver;
    }
	
	@Bean
    public TilesConfigurer tilesConfigurer(){
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions("/WEB-INF/layouts/layouts.xml",
										"/WEB-INF/views/**/views.xml");
		tilesConfigurer.setCheckRefresh(true);
		return tilesConfigurer;
	}
	
	@Bean
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}


	@Bean
	public RequestContextListener requestContextListener(){
		return new RequestContextListener();
	}


}
