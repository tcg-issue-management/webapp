package com.protoss.ims.app.spring.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.protoss.ims.app.constant.ApplicationConstant;
import com.protoss.ims.app.util.RestDataExternalContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Configuration
public class ApplicationInitializer implements ServletContextInitializer {

    static final Logger LOGGER = LoggerFactory.getLogger(ApplicationInitializer.class);

    @Value("${i18n.path}")
    private String buildPath = "";


    @Autowired
    RestDataExternalContext restDataExternalContext;


    public void onStartup(ServletContext container) throws ServletException {
        LOGGER.info("-= On Startup =-");
        System.setProperty("applicationName", "GWF");
        //springMessageBuilder(this.getClass().getResource("/").getPath()+"i18n");
        //System.setProperty("rootPath", this.getClass().getResource("/").getPath());

        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(ApplicationConfiguration.class);
        ctx.setServletContext(container);

        ServletRegistration.Dynamic servlet = container.addServlet("dispatcher", new DispatcherServlet(ctx));



        container.addListener(new RequestContextListener());
        container.addListener(new SessionListener());
        LOGGER.info("-= After add Listener =-");

        servlet.setLoadOnStartup(2);
        servlet.addMapping("/");


    }


    public void springMessageBuilder() {
        this.springMessageBuilder(buildPath);


    }

    public void springMessageBuilder(String realPath) {
        LOGGER.info("realPath : {}", realPath);
        LOGGER.info("buildPath : {}", buildPath);


        List<Map> parameterLang = restDataExternalContext.findByParameterCodeIn(ApplicationConstant.PARAMETER_LANGUAGE);
        List<Map> dataLocaleMessage = restDataExternalContext.getAllLocaleMessage();

        Map<String, Properties> propMap = new HashMap();
        for (Map data : dataLocaleMessage) {
            String groupCode = String.valueOf(data.get("groupCode"));

            if ("LB".equals(groupCode)) {
                groupCode = "label";
            } else if ("BT".equals(groupCode)) {
                groupCode = "button";
            } else {
                groupCode = "message";
            }

            for (int i = 0; i < parameterLang.size(); i++) {
                String langCode = String.valueOf(parameterLang.get(i).get("variable3"));
                Properties prop = propMap.get(groupCode + "_" + langCode + ".properties");
                if (prop == null) {
                    prop = new Properties();
                }
                prop.setProperty(String.valueOf(data.get("code")), String.valueOf(data.get("variableName" + (i + 1))));
                propMap.put(groupCode + "_" + langCode + ".properties", prop);
            }
        }

        for (String key : propMap.keySet()) {
            genSpringMessagePropertiesFile(propMap.get(key), buildPath + "/" + key);
        }


    }

    public void genSpringMessagePropertiesFile(Properties prop, String fileName) {
        OutputStream output = null;

        try {

            output = new FileOutputStream(fileName);

            // save properties to project root folder
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
