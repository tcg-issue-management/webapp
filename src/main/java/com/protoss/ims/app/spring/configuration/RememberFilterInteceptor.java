package com.protoss.ims.app.spring.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.protoss.ims.app.util.SerializeUtil;

public class RememberFilterInteceptor extends HandlerInterceptorAdapter{

	static final Logger LOGGER = LoggerFactory.getLogger(RememberFilterInteceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,  Object handler) throws Exception  {


		HandlerMethod hm = (HandlerMethod)handler;
		//GetMethod
		try {
			String[] getMapping = hm.getMethod().getAnnotation(GetMapping.class).value();
			String code  = request.getParameter("masterdataCode");
			if("null".equals(String.valueOf(code))){
				code  = request.getParameter("groupCode");
			}
			if("null".equals(String.valueOf(code))){
				code  = request.getParameter("locationType");
			}



			if(getMapping!=null && getMapping.length >= 1){
				String pageName = hm.getBeanType().getSimpleName().replace("Controller", "").toLowerCase();

				String cookieName = pageName+"-search-filter";
				if(code!=null && code.trim().length() > 0){
					cookieName = pageName+"_"+code+"-search-filter";
				}
				Map<String,String[]> paramMap = new HashMap<String,String[]>();
				for(String key:request.getParameterMap().keySet()){
					paramMap.put(key, request.getParameterMap().get(key));
				}
				Cookie cookie = new Cookie(cookieName,new  String(SerializeUtil.mapToStream(paramMap)));
				response.addCookie(cookie);
			}
		} catch (Exception e) { }

		//Post Method
		try {
			String[] postMapping = hm.getMethod().getAnnotation(PostMapping.class).value();
			String code  = request.getParameter("masterdataCode");
			if("null".equals(String.valueOf(code))){
				code  = request.getParameter("groupCode");
			}
			if("null".equals(String.valueOf(code))){
				code  = request.getParameter("locationType");
			}

			if(postMapping!=null && postMapping.length >= 1){
				String pageName = hm.getBeanType().getSimpleName().replace("Controller", "").toLowerCase();
				String cookieName = pageName+"-search-filter";
				if(code!=null && code.trim().length() > 0){
					cookieName = pageName+"_"+code+"-search-filter";
				}
				Map<String,String[]> paramMap = new HashMap<String,String[]>();
				for(String key:request.getParameterMap().keySet()){
					paramMap.put(key, request.getParameterMap().get(key));
				}
				Cookie cookie = new Cookie(cookieName,new  String(SerializeUtil.mapToStream(paramMap)));
				response.addCookie(cookie);
			}
		} catch (Exception e) { }


		return true;
	}

}
