package com.protoss.ims.app.spring.configuration;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        /*Set Session Time Out = 30 min*/
        event.getSession().setMaxInactiveInterval(240*60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
    }
}
