package com.protoss.ims.app.spring.localstorage;

//import com.spt.app.service.MasterDataService;
//import com.spt.app.service.ParameterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
        import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class CookieLocalStorageInteceptor extends HandlerInterceptorAdapter{

	
	static final Logger LOGGER = LoggerFactory.getLogger(CookieLocalStorageInteceptor.class);

//	@Autowired
//	ParameterService parameterService;
//
//	@Resource(name="MasterDataService")
//	MasterDataService masterDataService;
//
//	SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddhhmmss");
//	Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").create();
//
//
//	public void postHandle( HttpServletRequest request, HttpServletResponse response,  Object handler,  ModelAndView modelAndView) throws Exception  {
//		if (modelAndView != null && modelAndView.getModelMap() != null) {
//			HandlerMethod hm = (HandlerMethod)handler;
//
//			if(hm.getMethod().getAnnotation(GetMapping.class) !=null){
//				String[] getMapping = hm.getMethod().getAnnotation(GetMapping.class).value();
//				boolean isFetchData = true;
//				if(getMapping!=null && getMapping.length >= 1){
//					String cookieName = ApplicationConstant.FETCH_PARAMETER_NAME;
//					try{
//						for(Cookie cookie:request.getCookies()){
//							if(cookie.getName().equals(cookieName)
//									&& !"".equals(ApplicationConstant.FETCH_FRONTEND_PARAMETER_TIME)
//									&& cookie.getValue().equals(ApplicationConstant.FETCH_FRONTEND_PARAMETER_TIME) ){
//								isFetchData = false;
//								break;
//							}
//						}
//
//					}catch (NullPointerException e){
//						e.printStackTrace();
//						LOGGER.error("{}",e.getStackTrace());
//					}
//				}
//
//				if(isFetchData){
//					Cookie cookie = new Cookie(ApplicationConstant.FETCH_PARAMETER_NAME,ApplicationConstant.FETCH_FRONTEND_PARAMETER_TIME);
//					response.addCookie(cookie);
//
//                        /* Parameter */
//					Map<String,Object> parameterData = new HashMap();
//					parameterData.put("index", ApplicationConstant.PARAMETER_LOAD_LOCAL);
//					for(String paramCode :ApplicationConstant.PARAMETER_LOAD_LOCAL){
//						parameterData.put(paramCode, parameterService.findByParameterCodeIn(paramCode));
//					}
//					modelAndView.getModelMap().addAttribute("PARAMETER_FETCH", gson.toJson(parameterData));
//
//                        /* Master Data*/
//					Map<String,Object> masterData = new HashMap();
//					masterData.put("index", ApplicationConstant.MASTER_DATA_LOAD_LOCAL);
//					for(String masterCode :ApplicationConstant.MASTER_DATA_LOAD_LOCAL){
//						masterData.put(masterCode, masterDataService.findByMasterCodeIn(masterCode));
//					}
//					modelAndView.getModelMap().addAttribute("MASTER_DATA_FETCH", gson.toJson(masterData));
//
//
//				}
//			}
//			modelAndView.getModelMap().addAttribute("FETCH_FRONTEND_PARAMETER_TIME", ApplicationConstant.FETCH_FRONTEND_PARAMETER_TIME);
//		}
//	}

}
