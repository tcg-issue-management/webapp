package com.protoss.ims.app.spring.security;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@Configuration
public class DBAuthenticationProvider implements AuthenticationProvider {


    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


    static final Logger LOGGER = LoggerFactory.getLogger(DBAuthenticationProvider.class);

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    CustomUserModel customUserModel;

    @Value("${EngineServer}")
    protected String engineServer = ""; //http://172.16.13.1:9002/GWFEngine

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("========================== DBAuthenticationProvider ");
        String name = authentication.getName().toLowerCase();;
        String password = authentication.getCredentials().toString();
        customUserModel.addValue(CustomUserModel.ATTR_CUSTOM_PASSWORD, password);

        if (checkAuthentication(name, password)) {
            LOGGER.info("DB Cretential Success");
            // use the credentials
            // and authenticate against the third-party system
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            LOGGER.info("DB Cretential not found !!");
            return null;
        }
    }


    private boolean checkAuthentication(String userName, String rawPassword) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Content-Type", "application/json; charset=utf-8");
            headers.add("userName", userName);


	        /* Find User */
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String url = "/api/users/" + userName;
            LOGGER.info("EngineServer : "+engineServer);
            String json = restTemplate.exchange(engineServer + url, HttpMethod.GET, entity, String.class).getBody();
            Map user = gson.fromJson(json, Map.class);
            String accessToken = String.valueOf(user.get("accessToken"));

            LOGGER.info("accessToken : "+accessToken);
            LOGGER.info("rawPassword : "+rawPassword);
            LOGGER.info("rawPasswordencode : "+md5Hash(rawPassword.toString()));
            LOGGER.info("check : "+md5Hash(rawPassword.toString()).equals(accessToken));

            return md5Hash(rawPassword.toString()).equals(accessToken);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String md5Hash(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext.toString();
        } catch (Exception e) {//} catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
