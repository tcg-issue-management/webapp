package com.protoss.ims.app.spring.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.protoss.ims.app.util.StringUtil;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

//@Configuration
public class LDAPAuthenticationProvider {

//    static final Logger LOGGER = LoggerFactory.getLogger(LDAPAuthenticationProvider.class);
//
//    @Value("${url.ldap}")
//    String urlLdap = "ldap://10.1.2.1:389";
//    @Value("${postfix.ldap}")
//    String postfixLdap = "mitrphol.com,panelplus.co.th,mce.co.th,ust.co.th";
//
//    RestTemplate restTemplate = new RestTemplate();
//
//    @Value("${OrgSysServer}")
//    protected String orgSysServer =""; //http://10.25.68.183:8066/OSX
//
//    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
//        public Date deserialize(JsonElement json, Type typeOfT,
//                                JsonDeserializationContext context) throws JsonParseException {
//            return json == null ? null : new Date(json.getAsLong());
//        }
//    };
//
//    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
//
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        LOGGER.info("========================== LDAPAuthenticationProvider ");
//        String name = authentication.getName().toLowerCase();;
//        String password = authentication.getCredentials().toString();
//
//        boolean isLdapUser = false;
//        boolean isEssUser = false;
//        for (String mailGroup : postfixLdap.split(",")) {
//            isLdapUser = checkLdapAuthentication(name, password, mailGroup);
//            isEssUser = checkUserOSX(name);
//            if (isLdapUser && isEssUser) break;
//        }
//
//        if (isLdapUser) {
//            LOGGER.info("AD Cretential Success");
//            // use the credentials
//            // and authenticate against the third-party system
//            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
//        } else if(isEssUser){
//            LOGGER.error("Data User not found !!");
//            return null;
//        }else {
//            LOGGER.error("AD Cretential not found !!");
//            return null;
//        }
//    }
//
//
//    private boolean checkLdapAuthentication(String name, String password, String postfix) {
//        //LOGGER.info("in ldapAuthen");
//        String url = urlLdap;
//        String username = name + "@" + postfix;
//
//        Hashtable<Object, Object> env = new Hashtable<Object, Object>();
//        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//        env.put(Context.PROVIDER_URL, url);
//        env.put(Context.SECURITY_AUTHENTICATION, "simple");
//        env.put(Context.SECURITY_PRINCIPAL, username);
//        env.put(Context.SECURITY_CREDENTIALS, password);
//        env.put(Context.REFERRAL, "follow");
//
//        try {
//            InitialLdapContext initialLdapContext = new InitialLdapContext(env, null);
//            return true;
//        } catch (NamingException e) {
//            return false;
//        }
//    }
//
//    private boolean checkUserOSX(String userName) {
//        boolean isUserOsx = false;
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//        headers.add("userName", userName);
//
//
//        /* Find User */
//        HttpEntity<String> entity = new HttpEntity<String>("", headers);
//        String url = "/profile/" + userName;
//        String json = restTemplate.exchange(orgSysServer + url, HttpMethod.GET, entity, String.class).getBody();
//        Map userMap = gson.fromJson(json, Map.class);
//        String dataProfile = String.valueOf(userMap.get("profile")).replaceAll("Corp.,", "Corp").replaceAll("Co.,", "Co ")
//                .replaceAll("Safety,H","Safety H").replaceAll("Safety , H","Safety H");
//        dataProfile = StringUtil.trimStringByString(dataProfile, "{", "}");
//        Map<String, String> userProfileMap = Splitter.on(", ").withKeyValueSeparator("=").split(dataProfile);
//        String userNameOSX = userProfileMap.get("User_name");
//
//        if (userNameOSX != null && !"NONE".equals(userNameOSX)) {
//            isUserOsx = true;
//        }
//        return isUserOsx;
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(UsernamePasswordAuthenticationToken.class);
//    }

}
