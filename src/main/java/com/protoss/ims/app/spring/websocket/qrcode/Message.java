package com.protoss.ims.app.spring.websocket.qrcode;

public class Message {

	private String actionUser;
    private String qrcode;
    
	public String getActionUser() {
		return actionUser;
	}
	public void setActionUser(String actionUser) {
		this.actionUser = actionUser;
	}
	public String getQrcode() {
		return qrcode;
	}
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
    
    
    
    
}
