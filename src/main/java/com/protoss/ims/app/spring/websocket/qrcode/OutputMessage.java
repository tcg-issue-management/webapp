package com.protoss.ims.app.spring.websocket.qrcode;

public class OutputMessage {

	private String actionUser;
    private String qrcode;
    private String time;
	public String getActionUser() {
		return actionUser;
	}
	public void setActionUser(String actionUser) {
		this.actionUser = actionUser;
	}
	public String getQrcode() {
		return qrcode;
	}
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public OutputMessage(String actionUser, String qrcode, String time) {
		super();
		this.actionUser = actionUser;
		this.qrcode = qrcode;
		this.time = time;
	}

    
    

    
}
