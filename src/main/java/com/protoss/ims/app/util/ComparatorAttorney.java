package com.protoss.ims.app.util;

import java.util.Comparator;
import java.util.Map;

public class ComparatorAttorney implements Comparator<Map>{

	@Override
	public int compare(Map o1, Map o2) {

		Integer i1 = Integer.parseInt(String.valueOf(o1.get("attorneyId")));
		Integer i2 = Integer.parseInt(String.valueOf(o2.get("attorneyId")));
		
		if (i1 > i2)
            return -1; // highest value first
        if (i1 == i2)
            return 0;
        return 1;
        
	}

}
