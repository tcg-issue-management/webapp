package com.protoss.ims.app.util;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ratthanan-w on 5/10/2560.
 */
public class DateUtil {

    private static Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

    private static  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);



    public static Timestamp getCurrentDate() {

        Timestamp today = null;
        try {

            Date nowDate = Calendar.getInstance().getTime();
            Date dateConvert = new Date();
            DateTime dateTime = new DateTime(dateConvert);
            today = new Timestamp(dateTime.minusHours(7).toDate().getTime());
//            today = new Timestamp(nowDate.getTime());
        } catch (Exception e) {
            LOGGER.error("error msg : {} ", e);
            throw new RuntimeException(e);
        }
        return today;
    }

}
