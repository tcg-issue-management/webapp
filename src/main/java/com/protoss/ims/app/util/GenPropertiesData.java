package com.protoss.ims.app.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;


public class GenPropertiesData {

	public static void main(String[] args) throws Exception {

		Resource resourceEn = new FileSystemResource("/Users/se/Desktop/MyHome/WORKSPACE/MPGProject/GWF/src/main/webapp/WEB-INF/i18n/message_en.properties");
		Resource resourceth = new FileSystemResource("/Users/se/Desktop/MyHome/WORKSPACE/MPGProject/GWF/src/main/webapp/WEB-INF/i18n/message_th.properties");
		Properties prop = PropertiesLoaderUtils.loadProperties(resourceEn);
		Properties propTh = PropertiesLoaderUtils.loadProperties(resourceth);
		
		for(Object key:prop.keySet()){
			System.out.println("	LocaleMessage.add(new LocaleMessage(\"MS\",\""+key+"\",\""+prop.getProperty(String.valueOf(key))+"\",\""+propTh.getProperty(String.valueOf(key))+"\"));");
			
		}
	}

}
