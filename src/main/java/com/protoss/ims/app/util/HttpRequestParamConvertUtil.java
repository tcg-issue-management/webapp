package com.protoss.ims.app.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequestParamConvertUtil {

	static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestParamConvertUtil.class);
	
	public static Map toMap(Map<String,String[]> map) {
		

		Map<String, String> data = new HashMap<String, String>();  
	    if(map!=null){
	    	for(String key:map.keySet()){
	    		LOGGER.info("><><>< KEY : {} ",key);
	    		if(!"parentId".equals(key) && !"_csrf".equals(key)){
	    			data.put(key, map.get(key)[0]);
	    		}
	    	}
	    }
	    return data;
	}

    public static String toUrlParam(Map<String, String> map) {
		

		StringBuilder sb = new  StringBuilder();
		String result = "";
	    if(map!=null){
	    	for(String key:map.keySet()){
	    		if(!"parentId".equals(key) && !"_csrf".equals(key)){
	    			sb.append("&"+key+"="+String.valueOf(map.get(key)));
	    		}
	    	}
	    	
	    	if(sb.length() > 0){
	    		result = sb.substring(1);
	    	}
	    }
	    return result;
	}
	
}
