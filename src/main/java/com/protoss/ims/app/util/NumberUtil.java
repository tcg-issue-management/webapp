package com.protoss.ims.app.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.util.Base64;

public class NumberUtil {

	public static Long toLong( String s ) {
		Long ret = 0l;
		try {
			BigDecimal bd = new  BigDecimal(s);
			ret = bd.longValue();
		} catch (Exception e) { }
		
		return ret;
	}
	

	public static Integer toInteger( String s ) {
		Integer ret = 0;
		try {
			BigDecimal bd = new  BigDecimal(s);
			ret = bd.intValue();
		} catch (Exception e) { }
		
		return ret;
	}
	
	public static Integer toInteger( String s ,Integer defaultValue) {
		Integer ret = defaultValue;
		try {
			BigDecimal bd = new  BigDecimal(s);
			ret = bd.intValue();
		} catch (Exception e) { }
		
		return ret;
	}
}
