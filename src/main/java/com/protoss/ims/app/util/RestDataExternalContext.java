package com.protoss.ims.app.util;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Configuration
public class RestDataExternalContext {
    static final Logger LOGGER = LoggerFactory.getLogger(RestDataExternalContext.class);

    @Value("${EngineServer}")
    private String EngineServer = "";

    private RestTemplate restTemplate;
    private JsonParser parser = new JsonParser();

    JsonSerializer<Date> ser = new JsonSerializer<Date>() {
        public JsonElement serialize(Date src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


    public RestDataExternalContext() {
    }

    private ResponseEntity<String> getResultByExchange(String urlParam) {
        LOGGER.error("EngineServer : {}", EngineServer);

        restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = this.EngineServer + urlParam;
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }

    public List<Map> findByParameterCodeIn(String code) {
        String url = "/parameterDetails/search/findByParameterCodeIn?parameter=" + code;
        String jsonParameter = getResultByExchange(url).getBody();
        Map parameterMap = gson.fromJson(jsonParameter, Map.class);
        List<Map> contentsParameter = (List<Map>) parameterMap.get("content");
        return contentsParameter;
    }


    public List<Map> getAllLocaleMessage() {
        String url = "/localeMessages?size=10000";
        String jsonParameter = getResultByExchange(url).getBody();
        Map parameterMap = gson.fromJson(jsonParameter, Map.class);
        List<Map> contentsParameter = (List<Map>) parameterMap.get("content");
        return contentsParameter;
    }

}
