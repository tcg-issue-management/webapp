package com.protoss.ims.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SortMapUtil {

	public static List<Map> sortData(List<Map> contents) {

		   List<Map> menuLs = new ArrayList<Map>();
	        Map map = new HashMap();
	        
	        //find sequence
	        for (int i = 0, j = 0; i < contents.size(); i++) {
	            map.put((j++), contents.get(i).get("name") + ":" + find(contents.get(i), contents, ""));
	        }

	        //Sort Sequence
	        int length = contents.size();
	        for (int i = 0; i < length; i++) {
	            for (int j = i; j < length - 1; j++) {
	                String s1 = (String) map.get(j).toString().substring(map.get(j).toString().indexOf(":") + 1);
	                s1 = s1.replaceFirst(s1.charAt(0) + "", s1.charAt(0) + ".0");
	                String s2 = (String) map.get(j + 1).toString().substring(map.get(j + 1).toString().indexOf(":") + 1);
	                s2 = s2.replaceFirst(s2.charAt(0) + "", s2.charAt(0) + ".0");
	                float f1 = Float.valueOf(s1);
	                float f2 = Float.valueOf(s2);

	                if (f1 > f2) {
	                    String temp = (String) map.get(j);
	                    map.put(j, (String) map.get(j + 1));
	                    map.put(j + 1, temp);

	                }
	            }
	        }

	        //Add to List
	        for (int i = 0; i < contents.size(); i++) {
	            String s = map.get(i).toString().substring(map.get(i).toString().indexOf(":") + 1);

	            String name = map.get(i).toString().substring(0, map.get(i).toString().indexOf(":"));
	            for (int j = 0; j < contents.size(); j++) {

	                if (contents.get(j).get("name").equals(name)) {
	                    Map maps = new HashMap();
	                    maps.put("id", contents.get(j).get("id"));
	                    maps.put("code", contents.get(j).get("code"));
	                    maps.put("name", contents.get(j).get("name"));
	                    maps.put("sequence", contents.get(j).get("sequence"));
	                    maps.put("value", find(contents.get(j), contents, ""));
	                    maps.put("layer", find(contents.get(j), contents, "").length());
	                    maps.put("flagHasChild", contents.get(j).get("flagHasChild"));
	                    maps.put("urlLink", contents.get(j).get("urlLink"));
	                    maps.put("privilege", contents.get(j).get("privilege"));
	                    maps.put("parent", contents.get(j).get("parent"));
	                    menuLs.add(maps);

	                }
	            }
	               
	        }
	            
	       
	        return menuLs;

	    }
		 
	   public static String find(Map map, List<Map> list, String sub) {
	        String temp = sub;
	        temp = temp.concat((String) map.get("sequence"));

	        for (int i = 0; i < list.size(); i++) {
	            if (list.get(i).get("id").equals(map.get("parent"))) {
	                if (list.get(i).get("parent").equals("0")) {
	                    temp = temp.concat((String) list.get(i).get("sequence"));
	                    String reverse = reverse(temp);
	                    //    System.out.println("After return "+reverse);
	                    return reverse;
	                } else {
	                    return find(list.get(i), list, temp);
	                }

	            }
	        }
	        return (String) map.get("sequence");
	    }

	  

	    public static String reverse(String input) {
	        char[] in = input.toCharArray();
	        int begin = 0;
	        int end = in.length - 1;
	        char temp;
	        while (end > begin) {
	            temp = in[begin];
	            in[begin] = in[end];
	            in[end] = temp;
	            end--;
	            begin++;
	        }
	        return new String(in);
	    }
		    
		    public static List<Map> editList(List<Map> contents ){
		    	
		    	for(int i=0; i<contents.size(); i++){
	            	 Double d = (Double) contents.get(i).get("id");
	            	 String str = String.valueOf(d);
	            	 str = str.substring(0, str.indexOf("."));
	            	 contents.get(i).put("id", str);
	            	 d = (Double) contents.get(i).get("sequence");
	            	 str = String.valueOf(d);
	            	 str = str.substring(0, str.indexOf("."));
	            	 contents.get(i).put("sequence", str);
	            	 
	             }
	             for(int i=0; i<contents.size(); i++){
	            	 Double d = (Double) contents.get(i).get("parent");
	            	 	if(d == null){
	            	 		contents.get(i).put("parent", "0");
	            	 	}else{
	            	 		 d = (Double) contents.get(i).get("parent");
	                    	String str = String.valueOf(d);
	                    	 str = str.substring(0, str.indexOf("."));
	                    	 contents.get(i).put("parent", str);
	            	 	}
	            	 
	             }
	             return contents;
		    	
		    }
}
