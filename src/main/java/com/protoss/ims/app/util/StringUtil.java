package com.protoss.ims.app.util;

public class StringUtil {
    public static  String trimStringByString(String text, String trimStart, String trimEnd) {
        int beginIndex = 0;
        int endIndex = text.length();

        while (text.substring(beginIndex, endIndex).startsWith(trimStart)) {
            beginIndex += trimStart.length();
        }

        while (text.substring(beginIndex, endIndex).endsWith(trimEnd)) {
            endIndex -= trimEnd.length();
        }

        return text.substring(beginIndex, endIndex);
    }
}
