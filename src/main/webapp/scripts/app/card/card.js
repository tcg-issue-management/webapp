let sizeGroup = 0;
var widthCard = '';

$(document).ready(function () {
    $.material.init();
    setStyleMenu()
    rednerCardList()
    $('#add_line_group').on('click',function () {
        addProjectGroup()
    })
})



function rednerCardList(){

var url_string = window.location.href
var url = new URL(url_string);
var id = url.searchParams.get("id");
    $ID_PROJECT = parseInt(id)
var cardList = $.ajax({
    url: session.context + "/card/findCardList?id="+parseInt($ID_PROJECT)   ,
    headers: {
        Accept: "application/json"
    },
    type: "GET",
    async: false
}).responseJSON;

if(cardList){
    $("#headColumn").empty()

    sizeGroup = cardList.length
    widthCard =  sizeGroup > 4 ? '300px' : '300px;'
    $.each(cardList,function (index,item) {
        $("#headColumn").append('' +
            '  <td  id="panel_card_'+index+'" style="width: 300px !important; text-align: center !important;">' +
                '<div  class="style-group-name" >'+
                    item.groupName+
                '</div>' +
            '</td>' +
            '');
        let selectorId = 'panel_card_'+index
        $('#'+selectorId).append(
            '<div onclick="addCard(\''+
            item.idProjectGroup+'\',\''+
            item.groupName+'\')" style="width: 300px !important;" class="border-item-white"><a>'+'+'+'</a></div>')

    });


    $.each(cardList,function (index,item) {
        let cardDetail = item.cardList.length
        // console.log('cardList length.. = '+cardDetail)

        if(cardDetail != 0){
            $.each(item.cardList,function (indexCard,cardDetail) {
                    // console.log(cardDetail.msg)
                let selectorCard = 'panel_card_'+index
                $('#'+selectorCard).append(
                    // '<div class="mails" style="height: 200px;">'+
                    //     '<div class="message-list scrollable">'+
                    //         '<div class="scrollable__target">'+
                                '<div onclick="detailCard(\''+
                                cardDetail.createdDate+'\',\''+
                                cardDetail.sender+'\',\''+
                                cardDetail.status+'\',\''+
                                cardDetail.msg+'\',\''+
                                cardDetail.id+'\')" style="width: 305px !important;" class="card-detail-style">'+
                                    '<div style="width: 310px !important;margin-left: -10px;" class="card-detail-style-top">'+
                                    'Header '+ /*header*/ 
                                    '</div>'+
                                        cardDetail.msg+
                                    '<div style="width: 310px !important;margin-left: -10px;" class="card-detail-style-bottom">'+
                                    'Footer'+ /*footer*/ 
                                    '</div>'+
                        //         '</div>'+
                        //     '</div>'+
                        // '</div>'+
                    '</div>')

            })

        }

    });





}




}



function setStyleMenu(){
    // console.log('setStyleMenu')
    $('#linkBoard').addClass('hilight-text')
}



async function addCard(idProjectGroup, groupName) {


    const {value: text} = await Swal.fire({
        title: 'Add issue card.',
        text: groupName,
        input: 'textarea',
        inputPlaceholder: 'Type your message here...',
        inputAttributes: {
            'aria-label': 'Type your message here'
        },showCancelButton: true
        
    });
    console.log('text is '+text)
    if(text != '' && text != undefined){
        var jsonData = {};                             
            jsonData['idProjectGroup']  = parseInt(idProjectGroup);
            jsonData['msg']     	    = encodeURIComponent(text);
            jsonData['parentId']     	= 1;
            jsonData['sender']     	= "ims";
            jsonData[csrfParameter]  	= csrfToken;

    var jsonCard = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        url: session['context'] + '/card/addCard',
        data : JSON.stringify(jsonData) ,
        async: false,
        complete: function (xhr) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            });
            if (xhr.readyState == 4) {
                if(xhr.getResponseHeader('statusValidate') != '-1'){
                    swalWithBootstrapButtons.fire(
                        'Success',
                        'Add Card success!!',
                        'success'
                    )
                }else{
                    swalWithBootstrapButtons.fire(
                        'Error',
                        'Invalid Code!!',
                        'error'
                    )
                }
            }
            rednerCardList()
        }
    });
    }

    

  

}


async function detailCard(date, sender, status, msg, id) {
    console.log('date '+date)
    console.log('sender '+sender)
    console.log('status '+status)
    console.log('msg '+msg)

    let statusColor = 'white'
    let statusOpen;
    let statusAssign;
    let statusClose;
    if(status == 'Open' || status == 'open' || status =='OPEN'){
        statusColor = '#0172bd'
         statusOpen = true;
    }else if (status == 'Assign' || status == 'assign' || status =='ASSIGN'){
        statusColor = '#ffff00'
         statusAssign = true;
    }else if (status == 'Close' || status == 'close' || status =='CLOSE'){
        statusColor = '#00ff00'
         statusClose = true;
    } 
    console.log('statusOpen = '+statusOpen==true)
    let backgroundSelected = '#00bcd4'

    Swal.fire({
        title: '<b>Issue Detail</b>',
        // type: 'info',
        html:
            '<div class ="swal-wide" >'+
            '<div class="row modal-style">Issue Date :'+(formatDate(new Date(parseInt(date))))+' </div>'+
            '<div class="row modal-style">Sender :' +
            ''+sender+''+
            '</div>'+
            // '<div class="row modal-style">Status : '+status+' <i style="color: '+statusColor+';" class="fa fa-circle" aria-hidden="true"><jsp:text/></i> '+''+
            '<div class="row modal-style">'+
            '<div class="col-sm-2" style="padding:0px;" >Status : </div>'+
            '<div onclick="changeStatusCard(\''+id+'\',\''+'open'+'\')" title="Change status to OPEN" class="col-sm-3 border-card-status '+(statusOpen == true ? 'backgroundSelected' : '')+' ">'+'Open'+' </div>'+
            '<div onclick="changeStatusCard(\''+id+'\',\''+'assign'+'\')" title="Change status to ASSIGN" class="col-sm-3 border-card-status '+(statusAssign == true ? 'backgroundSelected' : '')+' ">'+'Assign'+' </div>'+
            '<div onclick="changeStatusCard(\''+id+'\',\''+'close'+'\')" title="Change status to CLOSE" class="col-sm-3 border-card-status '+(statusClose == true ? 'backgroundSelected' : '')+' ">'+'Close'+' </div>'+
            '</div>'+
            '<div class="row modal-style">Message : '+msg+' </div>'+
            '</div>',
        
            
        customClass: 'swal-wide'

        // showCloseButton: true
        
      })
      $.material.init();

  

}

function formatDate(date) {
    var monthNames = [
      "01", "02", "03",
      "04", "05", "06", "07",
      "08", "09", "10",
      "11", "12"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  

    return year + '-' + monthNames[monthIndex] + '-' + day;
  }

function addProjectGroup(){
    Swal.mixin({
        input: 'text',
        confirmButtonText: 'OK',
        showCancelButton: true
        // progressSteps: ['1', '2', '3']
    }).queue([
        {
            title: 'Input Group code.'
            // text: 'Chaining swal2 modals is easy'
        }
    ]).then((result) => {
        if (result.value) {
            let groupCode = (result.value).toString()
            // console.log('result = '+result.value)
            var jsonData = {};
            jsonData['groupCode']     	= groupCode;
            jsonData['projectId']     	= parseInt($ID_PROJECT);
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                contentType: "application/json; charset=utf-8",
                url: session['context'] + '/projects/addProjectGroup',
                data : jsonData,
                async: false,
                complete: function (xhr) {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    });
                    if (xhr.readyState == 4) {
                        if(xhr.getResponseHeader('statusValidate') != '-1'){
                            swalWithBootstrapButtons.fire(
                                'Success',
                                'Add Group success!!',
                                'success'
                            )
                        }else{
                            swalWithBootstrapButtons.fire(
                                'Error',
                                'Invalid Code!!',
                                'error'
                            )
                        }
                    }
                    rednerCardList()
                }
            });

        }
    })

}


function changeStatusCard(id, status){
    console.log('changeStatusCard id = '+id+' status = '+status)
}