$(document).ready(function () {
 $.material.init();
console.log('exportExcel js')
    setStyleMenu()



    $('#generateReport').on('click',function(){
        generateReportExcel()
    })




    $("#search_tim_start").flatpickr({
        dateFormat : "Y-m-d",
        enableTime : true
    });

    $("#imageCalendarSearch_tim_start").on('click',function () {
        $("#search_tim_start").focus()
    });

    $("#search_tim_end").flatpickr({
        dateFormat : "Y-m-d",
        enableTime : true
    });

    $("#imageCalendarSearch_tim_end").on('click',function () {
        $("#search_tim_end").focus()
    });

})

function setStyleMenu(){
    var url_string = window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    $ID_PROJECT = parseInt(id)
    console.log('setStyleMenu')
    $('#linkExport').addClass('hilight-text')
}


function generateReportExcel(){
    let startDate = $("#search_tim_start").val()=="" ? "": (new Date($("#search_tim_start").val()).getTime()).toString()
    let endDate = $("#search_tim_end").val()=="" ? "": (new Date($("#search_tim_end").val()).getTime()).toString()
    let status = $( "#statusList  option:selected" ).text()
    window.location.href = session.context + '/report/generateReport?id='+parseInt($ID_PROJECT)+'&startDate='+startDate+'&endDate='+endDate+'&status='+status
}


