var $ID_PROJECT;

$(document).ready(function () {
    setStyleMenu();
    renderIssue();

    $("#statusIssue").on('change',function () {
        renderIssue();
    });
});

function setStyleMenu(){
    var url_string = window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    $ID_PROJECT = parseInt(id)
    console.log('setStyleMenu')
    $('#linkIssueList').addClass('hilight-text')
}

function renderIssue() {

    var url_string = window.location.href
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    $ID_PROJECT = parseInt(id);
    var status = $("#statusIssue").val();
    var cardList = $.ajax({
        url: session.context + "/card/findCardsByProjectIdAndStatus?id="+parseInt($ID_PROJECT)+"&status="+status  ,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    var record = '';
    $("#bodyGrid").empty();
    if(cardList){
        $.each(cardList,function (index,item) {
            if(item.cardList.length > 0){
                $.each(item.cardList,function (index2,item2) {
                    console.log(item2.msg.length)
                    console.log(item2.msg.length > 70)
                    console.log(item2.msg.substring(0,70))
                    var msg = item2.msg.length > 70 ? item2.msg.substring(0,70) : item2.msg;
                    var $IMAGE_TICK;
                    if(item2.status == 'open'){
                        $IMAGE_TICK = $CircleBlue;
                    }else if(item2.status == 'assign'){
                        $IMAGE_TICK = $CircleGreen
                    }else{
                        $IMAGE_TICK = $CircleRed
                    }

                    record += '' +
                        '<div class="row">' +
                            '<div class="col-sm-12">' +
                                '<div class="col-sm-1 col-sm-offset-1" style="padding-top: 10px; padding-left: 0; padding-right: 0; text-align: center">' +
                                    '<img src=' + $IMAGE_TICK + ' width="35px"/>&#160;' +
                                '</div>' +
                                '<div class="col-sm-1" style="text-align: center; padding-top: 0px;"> ' +
                                    '<div class="col-sm-12"> '+'['+item2.status+']'+ '</div>' +
                                '</div>' +
                                '<div class="col-sm-6" style="text-align: left; padding-left: 0">' +
                                    '<div class="col-sm-12" style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;">' +
                                        msg +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-sm-2" style="text-align: left; padding-top: 0px; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-sm-12">' + item2.sender + '</div>' +
                                    '<div class="col-sm-12">' + new Date(item2.createdDate).format("yyyy-mm-dd HH:mm") + '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';
                });
            }
        });
    }

    $("#bodyGrid").append(record);
}