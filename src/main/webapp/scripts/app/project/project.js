$(document).ready(function () {
console.log('project js')
    setPermissionMenu()
    renderProject();

    $('#addProject').on('click',function () {
        addProject()
    })




})



function setPermissionMenu() {

    $('#linkBoard').hide();
    $('#linkIssueList').hide();
    $('#linkUserList').hide();
    $('#linkExport').hide();

}

function renderProject() {
    $("#cardProject").empty();
    var project = $.ajax({
        url: session.context + "/projects/findAllProject",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(project){
        $.each(project,function (index,item) {
            $("#cardProject").append('<div class="col-xs-6 col-md-3">' +
                '        <a style="border-radius: 20px;border: 0px solid #ddd;" href="'+ session.context +'/card/list?id='+item.id+'" class="thumbnail">' +
                '          <img src="'+$IMAGE_PROJECT+'" alt="'+item.projectName+'"/>' +
                '          <div class="caption" style="font-size: 20px;color: #03a9f4;">' +
                '            <p style="text-align: center;">'+item.projectName+'</p>' +
                '         </div>' +
                '       </a>' +
                '      </div>');
        })
        
        

        $("#cardProject").append('<div id="addProject" class="col-xs-6 col-md-3 ">' +
            '        <a style="border-radius: 20px;"  class="thumbnail AddItemSquircle-box">' +
            '          <img src="'+$IMAGE_PLUS+'" alt="Add Project"/>' +
            '          <div class="caption" style="font-size: 24px;font-family: sans-serif;font-weight: 900;color: #9da7b1;">' +
            '            <p style="text-align: center;font-family: sans-serif !important;">'+'Add Project'+'</p>' +
            '         </div>' +
            '       </a>' +
            '      </div>');
    }
}



function addProject() {
    Swal.mixin({
        input: 'text',
        confirmButtonText: 'OK',
        showCancelButton: true
        // progressSteps: ['1', '2', '3']
    }).queue([
        {
            title: 'Input Project name.'
            // text: 'Chaining swal2 modals is easy'
        }
    ]).then((result) => {
        if (result.value) {
            let projectname = (result.value).toString()
            console.log('result = '+result.value)
            var jsonData = {};
            jsonData['projectName']     	= projectname;
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                contentType: "application/json; charset=utf-8",
                url: session['context'] + '/projects/addProject',
                data : jsonData,
                async: false,
                complete: function (xhr) {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    });
                    if (xhr.readyState == 4) {

                        if(xhr.getResponseHeader('statusValidate') != '-1'){
                            swalWithBootstrapButtons.fire(
                                'Success',
                                'Add Group success!!',
                                'success'
                            )
                        }else{
                            swalWithBootstrapButtons.fire(
                                'Error',
                                'Invalid Code!!',
                                'error'
                            )
                        }
                    }
                    renderProject()
                }
            });

        }
    })
}