$(document).ready(function () {
    renderProjectMenu()
});


function submitlogout() {
    console.log('logout')
    location.href = session.context+'/login?logout'
}

function goHome(){
    window.location.href = session.context + '/'
}
function openSideMenu(){
    document.getElementById('side-menu').style.width ='250px';
    // document.getElementById('main').style.marginLeft ='250px';
}

function closeSideMenu(){
    document.getElementById('side-menu').style.width ='0';
    // document.getElementById('main').style.marginLeft ='0';
}

function goBoard(){
    console.log('goBoard')
    window.location.href = session.context + '/card/list?id='+$ID_PROJECT
}

function goIssueListing(){
    console.log('goIssueListing')
    window.location.href = session.context + '/issue/list?id='+$ID_PROJECT
    /**/
}
function goUserGroup(){
    console.log('goUserGroup')
    window.location.href = session.context + '/userLine/list?id='+$ID_PROJECT
    /**/
}

function goKeywordSetup(){
    console.log('goKeywordSetup')
    window.location.href = session.context + '/keyword/list'
    /**/
}

function goExportExcel(){
    console.log('goExportExcel')
    window.location.href = session.context + '/exportExcel/list?id='+$ID_PROJECT
    /**/
}


function renderProjectMenu() {
    $("#cardProject").empty();
    var project = $.ajax({
        url: session.context + "/projects/findAllProject",
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(project){
        $.each(project,function (index,item) {
            $("#side-menu").append('' +
                '<a style="padding-left: 45px;" href="'+ session.context +'/card/list?id='+item.id+'" class="style-slide-menu"><i style="color: #3a20ac" class="fa fa-circle" aria-hidden="true"><jsp:text/>&#160;</i>'+item.projectName+'</a>' +
                '');
        })

        $("#side-menu").append('' +
            '<a style="font-size: 18px !important; font-weight: 700; cursor: default !important;">Report</a>' +
            '<a style="padding-left: 45px;" class="style-slide-menu"><i style="color: #3a20ac" class="fa fa-circle" aria-hidden="true"><jsp:text/>&#160;</i>'+'Daily Status'+'</a>' +
            '<a style="padding-left: 45px;" class="style-slide-menu"><i style="color: #3a20ac" class="fa fa-circle" aria-hidden="true"><jsp:text/>&#160;</i>'+'Monthly Status'+'</a>' +
            '<a style="padding-left: 45px;" class="style-slide-menu"><i style="color: #3a20ac" class="fa fa-circle" aria-hidden="true"><jsp:text/>&#160;</i>'+'Yearly Status'+'</a>' +

            '');






        $("#side-menu").append('' +
            '<a class="style-logout-menu" onclick="submitlogout()"><i class="fa fa-sign-out" aria-hidden="true"><jsp:text/>&#160;</i>Logout</a>' +
            '');



    }
}